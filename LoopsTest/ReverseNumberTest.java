package com.HW_1.Loops;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ReverseNumberTest {

    @Test
    void reverseNumber() {
        int result = ReverseNumber.reverseNumber(6354);
        assertEquals(result, 4536);
    }
}