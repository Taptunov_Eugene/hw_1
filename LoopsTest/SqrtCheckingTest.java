package com.HW_1.Loops;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SqrtCheckingTest {

    @Test
    void sqrtCoherentChecking() {
        int result = SqrtChecking.sqrtCoherentChecking(24);
        assertEquals(result, 4);
    }

    @Test
    void sqrtBinaryChecking() {
        int result = SqrtChecking.sqrtBinaryChecking(24);
        assertEquals(result, 5);
    }
}