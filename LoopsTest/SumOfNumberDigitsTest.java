package com.HW_1.Loops;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SumOfNumberDigitsTest {

    @Test
    void sumOfNumberDigits() {
        int result = SumOfNumberDigits.sumOfNumberDigits(32478);
        assertEquals(result, 24);
    }
}