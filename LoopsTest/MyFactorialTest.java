package com.HW_1.Loops;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MyFactorialTest {

    @Test
    void myFactorial() {
        int result = MyFactorial.myFactorial(6);
        assertEquals(result, 720);
    }
}