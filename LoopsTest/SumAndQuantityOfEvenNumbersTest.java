package com.HW_1.Loops;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class SumAndQuantityOfEvenNumbersTest {

    @Test
    void sumAndQuantityOfEvenNumbers() {
        String result = SumAndQuantityOfEvenNumbers.sumAndQuantityOfEvenNumbers();
        assertEquals(result, "[2450, 49]");

    }
}