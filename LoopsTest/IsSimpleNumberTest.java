package com.HW_1.Loops;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class IsSimpleNumberTest {

    @Test
    void isSimpleNumber() {
        String result = IsSimpleNumber.isSimpleNumber(21);
        assertEquals(result, "Число 21 не является простым");

        String result1 = IsSimpleNumber.isSimpleNumber(11);
        assertEquals(result1, "Число 11 простое");
    }
}