package com.HW_1.Loops;

public class ReverseNumber {
    public static int reverseNumber(int number) {
        System.out.println("Введенное число n = " + number);
        int reverse = 0;
        while (number != 0) {
            reverse = reverse * 10 + number % 10;
            number /= 10;
        }
        return reverse;
    }

    public static void main(String[] args) {

        System.out.println("Зеркальное отображение числа n = "
                + reverseNumber(24351));
    }
}
