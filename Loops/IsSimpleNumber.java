package com.HW_1.Loops;

public class IsSimpleNumber {

    public static String isSimpleNumber(int number) {
        for (int i = 2; i < number; i++) {
            if (number % i == 0)
                return "Число " + number + " не является простым";
        }
        return "Число " + number + " простое";
    }

    public static void main(String[] args) {

        System.out.println(isSimpleNumber(25));
        System.out.println(isSimpleNumber(11));
    }
}
