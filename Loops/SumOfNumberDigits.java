package com.HW_1.Loops;

public class SumOfNumberDigits {
    public static int sumOfNumberDigits(int number) {
        int sum = 0;
        while (number > 0) {
            sum += number % 10;
            number /= 10;
        }
        return sum;
    }

    public static void main(String[] args) {

        System.out.println("Сумма цифр заданного числа равна "
                + sumOfNumberDigits(13243));
    }
}
