package com.HW_1.Loops;

import java.util.Arrays;

public class SumAndQuantityOfEvenNumbers {

    public static String sumAndQuantityOfEvenNumbers() {
        int sum = 0, count = 0;
        for (int i = 1; i < 99; i++) {
            if (i % 2 == 0)
            {
                sum += i;
                count++;
            }
        }
        return Arrays.toString(new int[] {sum, count});
    }
    public static void main(String[] args) {
        System.out.println("Сумма и количество четных чисел в диапазоне (1-99) "
                + sumAndQuantityOfEvenNumbers());
    }
}
