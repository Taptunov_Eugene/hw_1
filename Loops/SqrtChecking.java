package com.HW_1.Loops;

public class SqrtChecking {

    public static int sqrtCoherentChecking(int number) {
        int i = 1;
        while (i * i <= number)
            i++;
        return i - 1;
    }

    public static int sqrtBinaryChecking(int number) {
        int result = number;
        while (result * result > number)
            result /= 2;
        while (result * result < number)
            result++;
        return result;
    }

    public static void main(String[] args) {

        System.out.println("Корень натурального числа методом последовательного перебора равен "
                + sqrtCoherentChecking(23));
        System.out.println("Корень натурального числа методом бинарного поиска равен "
                + sqrtBinaryChecking(23));
    }
}
