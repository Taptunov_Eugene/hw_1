package com.HW_1.Loops;

public class MyFactorial {
    public static int myFactorial(int number) {
        int result = 1;
        for (int i = 1; i <= number; i++)
            result *= i;
        return result;
    }

    public static void main(String[] args) {

        System.out.println("Факториал заданного числа равен " + myFactorial(6));
    }
}
