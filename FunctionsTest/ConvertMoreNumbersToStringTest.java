package com.HW_1.Functions;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ConvertMoreNumbersToStringTest {

    @Test
    void WordsRus() {
        String  result = ConvertMoreNumbersToString.WordsRus(236435243);
        assertEquals(result, "двести тридцать шесть миллионов четыреста " +
                "тридцать пять тысяч двести сорок три ");
    }
}