package com.HW_1.Functions;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class GetDistanceTest {

    @Test
    void getDistance() {
        double  result = GetDistance.getDistance(1, 0, 3, 0);
        assertEquals(result, 2);
    }

}