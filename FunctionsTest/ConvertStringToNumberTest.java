package com.HW_1.Functions;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ConvertStringToNumberTest {

    @Test
    void convertStringToNumber() {
        int  result = ConvertStringToNumber.convertStringToNumber("четыреста пятьдесят восемь", 458);
        assertEquals(result, 458);
    }
}