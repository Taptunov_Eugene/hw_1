package com.HW_1.Functions;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class GetDayOfWeekTest {

    @Test
    void getDayOfWeek() {
        String  result = GetDayOfWeek.getDayOfWeek(2);
        assertEquals(result, "День недели под номером 2 это вторник");

        String  result1 = GetDayOfWeek.getDayOfWeek(4);
        assertNotEquals(result1, "День недели под номером 2 это вторник");
    }
}