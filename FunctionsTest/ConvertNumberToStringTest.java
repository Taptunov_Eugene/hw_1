package com.HW_1.Functions;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ConvertNumberToStringTest {

    @Test
    void convertNumberToString() {
        String  result = ConvertNumberToString.convertLessThanOneThousand(236);
        assertEquals(result, "двести тридцать шесть");
    }
}