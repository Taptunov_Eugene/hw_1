package com.HW_1.Arrays;

public class SumElementsOddIndex {
    public static int sumElementsOddIndex(int[] array) {
        int sum = 0;
        for (int i = 0; i < array.length; i++) {
            if (i % 2 != 0)
                sum += array[i];
        }
        return sum;
    }

    public static void main(String[] args) {

        System.out.println("Сумма элементов массива с нечетными индексами равна "
                + sumElementsOddIndex(new int[]{5, 4, 54, 776, 765, 223, 55}));
    }
}
