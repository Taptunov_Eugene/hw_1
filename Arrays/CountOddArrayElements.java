package com.HW_1.Arrays;

public class CountOddArrayElements {
    public static int countOddArrayElements(int[] array) {
        int count = 0;
        for (int i = 0; i < array.length; i++) {
            if(array[i] % 2 != 0)
                count++;
        }
        return count;
    }

    public static void main(String[] args) {
        System.out.println("Количество нечетных элементов данного массива "
                + countOddArrayElements(new int[]{5, 4, 54, 776, 765, 223, 55}));
    }
}
