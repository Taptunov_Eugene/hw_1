package com.HW_1.Arrays;

public class MaxElement {
    public static int maxElement(int[] array) {
        int max = array[0];
        for (int i = 0; i < array.length; i++) {
            if (max < array[i])
                max = array[i];
        }
        return max;
    }

    public static void main(String[] args) {

        System.out.println("Максимальный элемент данного массива равен "
                + maxElement(new int[]{43, 77, 123, 11}));
    }
}
