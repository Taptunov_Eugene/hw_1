package com.HW_1.Arrays;

import java.util.Arrays;

public class HeapSort {
    public static int[] heapSort(int[] arr) {
        int n = arr.length;
        for (int i = n / 2 - 1; i >= 0; i--)
            heap(arr, n, i);
        for (int i = n - 1; i > 0; i--) {
            int temp = arr[0];
            arr[0] = arr[i];
            arr[i] = temp;
            heap(arr, i, 0);
        }
        return arr;
    }

    private static void heap(int arr[], int n, int i) {
        int largest = i;
        int l = 2 * i + 1;
        int r = 2 * i + 2;
        if (l < n && arr[l] > arr[largest])
            largest = l;
        if (r < n && arr[r] > arr[largest])
            largest = r;
        if (largest != i) {
            int swap = arr[i];
            arr[i] = arr[largest];
            arr[largest] = swap;
            heap(arr, n, largest);
        }
    }

    public static void main(String[] args) {

        int[] array = {5, 4, 54, 776, 765, 223, 55};
        System.out.println("Исходный массив " + Arrays.toString(array));

        System.out.println("Отсортированный массив пирамидальной сортировкой "
                + Arrays.toString(heapSort(array)));
    }
}
