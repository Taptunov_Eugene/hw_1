package com.HW_1.Arrays;

import java.util.Arrays;

public class QuickSort {
    public static int[] quickSort(int[] source, int leftBorder, int rightBorder) {
        int leftMarker = leftBorder, rightMarker = rightBorder;
        int supportElement = source[(leftMarker + rightMarker) / 2];
        do {
            while (source[leftMarker] < supportElement)
                leftMarker++;
            while (source[rightMarker] > supportElement)
                rightMarker--;
            if (leftMarker <= rightMarker) {
                if (leftMarker < rightMarker) {
                    int tmp = source[leftMarker];
                    source[leftMarker] = source[rightMarker];
                    source[rightMarker] = tmp;
                }
                leftMarker++;
                rightMarker--;
            }
        } while (leftMarker <= rightMarker);

        if (leftMarker < rightBorder)
            quickSort(source, leftMarker, rightBorder);
        if (leftBorder < rightMarker)
            quickSort(source, leftBorder, rightMarker);
        return source;
    }

    public static void main(String[] args) {
        int[] array = {5, 4, 54, 776, 765, 223, 55};
        System.out.println("Исходный массив " + Arrays.toString(array));

        quickSort(array, 0, array.length - 1);
        System.out.println("Отсортированный массив методом быстрой сортировки "
                + Arrays.toString(array));
    }
}
