package com.HW_1.Arrays;

public class IndexMaxElement {
    public static int indexMaxElement(int[] array) {
        int max = array[0];
        int indexMax = 0;
        for (int i = 0; i < array.length; i++) {
            int num = array[i];
            if (max < num) {
                max = num;
                indexMax = i;
            }
        }
        return indexMax;
    }

    public static void main(String[] args) {
        System.out.println("Индекс максимального элемента данного массива равен "
                + indexMaxElement(new int[]{43, 77, 123, 11}));
    }
}
