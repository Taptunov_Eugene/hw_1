package com.HW_1.Arrays;

public class IndexMinElement {
    public static int indexMinElement(int[] array) {
        int min = array[0];
        int indexMin = 0;
        for (int i = 0; i < array.length; i++) {
            int num = array[i];
            if (min > num) {
                min = num;
                indexMin = i;
            }
        }
        return indexMin;
    }

    public static void main(String[] args) {
        System.out.println("Индекс минимального элемента данного массива равен "
                + indexMinElement(new int[]{43, 77, 123, 11}));
    }
}
