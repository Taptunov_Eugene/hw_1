package com.HW_1.Arrays;

import java.util.Arrays;

public class ReplaceHalfArray {
    public static int[] replaceHalfArray(int[] array) {
        System.out.println("Исходный массив " + Arrays.toString(array));

        int vert = array.length / 2 + array.length % 2;
        for (int i = 0; i < vert; i++) {
            int currentElement = array[i];
            array[i] = array[i + vert];
            array[i + vert] = currentElement;
        }
        return array;
    }

    public static void main(String[] args) {

        System.out.println("Массив после перестановки первой и второй половины "
                + Arrays.toString(replaceHalfArray(new int[]{5, 6, 4, 54, 776, 765, 223, 55})));
    }
}
