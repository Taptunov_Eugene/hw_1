package com.HW_1.Arrays;

import java.util.Arrays;

public class ReverseArray {
    public static int[] reverseArray(int[] array) {
        System.out.println("Исходный массив " + Arrays.toString(array));
        int result[] = new int[array.length];
        int currentIndex = array.length - 1;
        for (int i = 0; i <= (array.length - 1); currentIndex--, i++)
        {
            result[currentIndex] = array[i];
        }
        return result;
    }

    public static void main(String[] args) {

        System.out.println("Массив в обратном направлении "
                + Arrays.toString(reverseArray(new int[]{5, 4, 54, 776, 765, 223, 55})));
    }
}
