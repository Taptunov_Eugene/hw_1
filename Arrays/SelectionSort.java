package com.HW_1.Arrays;

import java.util.Arrays;

public class SelectionSort {
    public static int[] selectionSort(int[] array) {
        System.out.println("Исходный массив " + Arrays.toString(array));

        for (int min = 0; min < array.length; min++) {
            int least = min;
            for (int j = min + 1; j < array.length; j++) {
                if (array[j] < array[least]) {
                    least = j;
                }
            }
            int currentElement = array[min];
            array[min] = array[least];
            array[least] = currentElement;
        }
        return array;
    }

    public static void main(String[] args) {
        System.out.println("Отсортированный массив сортировкой выбором "
                + Arrays.toString(selectionSort(new int[]{5, 4, 54, 776, 765, 223, 55})));
    }
}
