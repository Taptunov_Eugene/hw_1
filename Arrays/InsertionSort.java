package com.HW_1.Arrays;

import java.util.Arrays;

public class InsertionSort {
    public static int[] insertionSort(int[] array) {
        System.out.println("Исходный массив " + Arrays.toString(array));

        for (int left = 0; left < array.length; left++) {
            int value = array[left];
            int i = left - 1;
            for (; i >= 0; i--) {
                if (value < array[i])
                    array[i + 1] = array[i];
                else break;
            }
            array[i + 1] = value;
        }
        return array;
    }

    public static void main(String[] args) {
        System.out.println("Отсортированный массив вставками "
                + Arrays.toString(insertionSort(new int[]{5, 4, 54, 776, 765, 223, 55})));
    }
}
