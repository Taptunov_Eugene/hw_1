package com.HW_1.Arrays;

public class MinElement {
    public static int minElement(int[] array) {
        int min = array[0];
        for (int i = 0; i < array.length; i++) {
            if (min > array[i])
                min = array[i];
        }
        return min;
    }

    public static void main(String[] args) {

        System.out.println("Минимальный элемент данного массива равен "
                + minElement(new int[]{43, 77, 123, 11}));
    }
}
