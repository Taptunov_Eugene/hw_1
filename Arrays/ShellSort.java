package com.HW_1.Arrays;

import java.util.Arrays;

public class ShellSort {
    public static int[] shellSort(int[] arr) {
        int n = arr.length;
        for (int gap = n / 2; gap > 0; gap /= 2) {
            for (int i = gap; i < n; i += 1) {
                int temp = arr[i];
                int j;
                for (j = i; j >= gap && arr[j - gap] > temp; j -= gap) {
                    arr[j] = arr[j - gap];
                }
                arr[j] = temp;
            }
        }
        return arr;
    }

    public static void main(String[] args) {

        int[] array = {5, 4, 54, 776, 765, 223, 55};
        System.out.println("Исходный массив " + Arrays.toString(array));

        System.out.println("Отсортированный массив сортировкой Шелла "
                + Arrays.toString(shellSort(array)));
    }
}
