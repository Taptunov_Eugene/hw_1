package com.HW_1.Arrays;

import java.util.Arrays;

public class BubbleSort {
    public static int[] bubbleSort(int[] array) {
        System.out.println("Исходный массив " + Arrays.toString(array));

        boolean isSorted = false;
        while (!isSorted) {
            isSorted = true;
            for (int i = 1; i < array.length; i++) {
                if (array[i - 1] > array[i]) {
                    int currentElement = array[i];
                    array[i] = array[i - 1];
                    array[i - 1] = currentElement;
                    isSorted = false;
                }
            }
        }
        return array;
    }

    public static void main(String[] args) {
        System.out.println("Отсортированный массив методом пузырька "
                + Arrays.toString(bubbleSort(new int[]{5, 4, 54, 776, 765, 223, 55})));
    }
}
