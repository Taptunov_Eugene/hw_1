package com.HW_1.Conditionals;

public class StudentRatingMark {
    public static String studentRatingMark(int mark) {
        String rating = null;
        if (mark >= 0 && mark < 20)
            rating = "F";
        if (mark > 19 && mark < 40)
            rating = "E";
        if (mark > 39 && mark < 60)
            rating = "D";
        if (mark > 59 && mark < 75)
            rating = "C";
        if (mark > 74 && mark < 90)
            rating = "B";
        if (mark > 89)
            rating = "A";
        return "Рейтинг студента для оценки " + mark + " балов соответствует " + rating;
    }

    public static void main(String[] args) {

        System.out.println(studentRatingMark(67));
    }
}
