package com.HW_1.Conditionals;

public class CalculateExpression {
    public static int calculateExpression(int a, int b, int c) {
        int sum = a + b + c, multiply = a * b * c;
        if (multiply >= sum)
            return multiply + 3;
        else return sum + 3;
    }

    public static void main(String[] args) {

        System.out.println(calculateExpression(3, 7, 2));
    }
}
