package com.HW_1.Conditionals;

public class SumOfPositiveNumbers {
    public static int sumOfPositiveNumbers(int a, int b, int c) {
        int sum = 0;
        if (a > 0)
            sum += a;
        if (b > 0)
            sum += b;
        if (c > 0)
            sum += c;
        return sum;
    }

    public static void main(String[] args) {

        System.out.println("Сумма только положительных чисел равна "
                + sumOfPositiveNumbers(3, -7, -4));
    }
}
