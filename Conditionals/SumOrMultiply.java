package com.HW_1.Conditionals;

public class SumOrMultiply {

    public static int sumOrMultiply(int a, int b) {
        if (a % 2 == 0)
            return a * b;
        else return a + b;
    }

    public static void main(String[] args) {
        System.out.println(sumOrMultiply(3, 6));
        System.out.println(sumOrMultiply(4, 6));
        System.out.println(sumOrMultiply(0, 6));
    }
}
