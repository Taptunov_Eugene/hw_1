package com.HW_1.Conditionals;

public class FindQuarter {

    public static int findQuarter(int x, int y) {
        if (x > 0 && y > 0) {
            System.out.println("Точка с координатами x=" + x + ", y=" + y
                    + " принадлежит четверти №");
            return 1;
        } else if (x < 0 && y > 0) {
            System.out.println("Точка с координатами x=" + x + ", y=" + y
                    + " принадлежит четверти №");
            return 2;
        } else if (x < 0 && y < 0) {
            System.out.println("Точка с координатами x=" + x + ", y=" + y
                    + " принадлежит четверти №");
            return 3;
        } else if (x > 0 && y < 0) {
            System.out.println("Точка с координатами x=" + x + ", y=" + y
                    + " принадлежит четверти №");
            return 4;
        }
        return Integer.parseInt(null);
    }

    public static void main(String[] args) {

        System.out.println(findQuarter(3, -5));
    }
}
