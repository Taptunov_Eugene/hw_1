package com.HW_1.Arrays;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CountOddArrayElementsTest {

    @Test
    void countOddArrayElements() {
        int result = CountOddArrayElements.countOddArrayElements(new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10});
        assertEquals(result, 5);
    }
}