package com.HW_1.Arrays;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MinElementTest {

    @Test
    void minElement() {
        int result = MinElement.minElement(new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10});
        assertEquals(result, 1);
    }
}