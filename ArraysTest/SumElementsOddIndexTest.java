package com.HW_1.Arrays;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SumElementsOddIndexTest {

    @Test
    void sumElementsOddIndex() {
        int result = SumElementsOddIndex.sumElementsOddIndex(new int[]{5, 4, 54, 776, 765, 223, 55});
        assertEquals(result, 1003);
    }
}