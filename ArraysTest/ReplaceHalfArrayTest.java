package com.HW_1.Arrays;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class ReplaceHalfArrayTest {

    @Test
    void replaceHalfArray() {
        int[] result = ReplaceHalfArray.replaceHalfArray(new int[]{5, 6, 4, 54, 776, 765, 223, 55});
        assertTrue(Arrays.equals(result, new int[]{776, 765, 223, 55, 5, 6, 4, 54}));
    }
}