package com.HW_1.Arrays;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class ReverseArrayTest {

    @Test
    void reverseArray() {
        int[] result = ReverseArray.reverseArray(new int[]{5, 4, 54, 776, 765, 223, 55});
        assertTrue(Arrays.equals(result, new int[]{55, 223, 765, 776, 54, 4, 5}));
    }
}