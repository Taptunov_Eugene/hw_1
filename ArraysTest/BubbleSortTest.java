package com.HW_1.Arrays;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertTrue;

class BubbleSortTest {

    @Test
    void bubbleSort() {
        int[] result = BubbleSort.bubbleSort(new int[]{5, 4, 54, 776, 765, 223, 55});
        assertTrue(Arrays.equals(result, new int[]{4, 5, 54, 55, 223, 765, 776}));
    }
}