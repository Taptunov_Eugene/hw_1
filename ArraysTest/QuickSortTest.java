package com.HW_1.Arrays;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class QuickSortTest {

    @Test
    void quickSort() {
        int[] array = new int[]{5, 4, 54, 776, 765, 223, 55};
        int[] result = QuickSort.quickSort(array, 0, array.length - 1);
        assertTrue(Arrays.equals(result, new int[]{4, 5, 54, 55, 223, 765, 776}));
        assertFalse(Arrays.equals(result, new int[]{776, 765, 223, 55, 54, 5, 4}));
    }
}