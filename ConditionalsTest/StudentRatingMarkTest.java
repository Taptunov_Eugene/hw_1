package com.HW_1.Conditionals;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StudentRatingMarkTest {

    @Test
    void studentRatingMark() {
        String result = StudentRatingMark.studentRatingMark(19);
        assertEquals(result, "F");

        String result1 = StudentRatingMark.studentRatingMark(40);
        assertEquals(result1, "D");

        String result2 = StudentRatingMark.studentRatingMark(60);
        assertEquals(result2, "C");
    }
}