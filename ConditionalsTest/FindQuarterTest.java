package com.HW_1.Conditionals;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FindQuarterTest {

    @Test
    void findQuarter() {
        int result = FindQuarter.findQuarter(1,2);
        assertEquals(result, 1);

        int result1 = FindQuarter.findQuarter(-4, 6);
        assertEquals(result1, 2);

        int result2 = FindQuarter.findQuarter(-2, -6);
        assertEquals(result2, 3);
    }
}