package com.HW_1.Conditionals;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CalculateExpressionTest {

    @Test
    void calculateExpression() {
        int result = CalculateExpression.calculateExpression(10, -10, 10);
        assertEquals(result, 13);

        int result1 = CalculateExpression.calculateExpression(3, 1, 2);
        assertEquals(result1, 9);

        int result2 = CalculateExpression.calculateExpression(0, 1, 4);
        assertEquals(result2, 8);
    }
}