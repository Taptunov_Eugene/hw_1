package com.HW_1.Conditionals;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SumOrMultiplyTest {

    @Test
    void sumOrMultiply() {
        int result = SumOrMultiply.sumOrMultiply(3, 6);
        assertEquals(result, 9);

        int result1 = SumOrMultiply.sumOrMultiply(4, 6);
        assertEquals(result1, 24);

        int result2 = SumOrMultiply.sumOrMultiply(0, 6);
        assertEquals(result2, 0);
    }
}