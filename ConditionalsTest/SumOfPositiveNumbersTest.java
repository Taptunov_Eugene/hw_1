package com.HW_1.Conditionals;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SumOfPositiveNumbersTest {

    @Test
    void sumOfPositiveNumbers() {
        int result = SumOfPositiveNumbers.sumOfPositiveNumbers(10, 7, -10);
        assertEquals(result, 17);

        int result1 = SumOfPositiveNumbers.sumOfPositiveNumbers(-10, -20, -10);
        assertEquals(result1, 0);

        int result2 = SumOfPositiveNumbers.sumOfPositiveNumbers(-3, 4, 7);
        assertEquals(result2, 11);
    }
}