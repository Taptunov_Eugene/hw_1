package com.HW_1.Functions;

import java.util.Scanner;

public class ConvertNumberToString {
    protected static String convertLessThanOneThousand(int number) {
        String[] BELOW_TWENTY = {"ноль", "один", "два", "три", "четыре", "пять",
                "шесть", "семь", "восемь", "девять", "десять", "одиннадцать",
                "двенадцать", "тринадцать", "четырнадцать", "пятнадцать",
                "шестнадцать", "семнадцать", "восемнадцать", "девятнадцать"};
        String[] TENS = {"сто", "десять", "двадцать", "тридцать", "сорок",
                "пятьдесят", "шестьдесят", "семьдесят", "восемьдесят", "девяносто"};
        String[] HUNDREDS = {"тысяча", "сто", "двести", "триста", "четыреста",
                "пятьсот", "шестьсот", "семьсот", "восемьсот", "девятьсот"};
        String soFar;

        if (number % 100 < 20 && number != 100) {
            soFar = BELOW_TWENTY[number % 100];
            number /= 100;
        } else {
            if (number == 100) {
                return (TENS[0]);
            }
            soFar = BELOW_TWENTY[number % 10];
            number /= 10;

            soFar = TENS[number % 10] + " " + soFar;
            number /= 10;
        }
        if (number == 0) return soFar;
        return HUNDREDS[number] + " " + soFar;
    }

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.print("Введите число от 0 до 999: ");

        int number = sc.nextInt();
        System.out.println(convertLessThanOneThousand(number));
    }
}
