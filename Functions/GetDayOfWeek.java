package com.HW_1.Functions;

public class GetDayOfWeek {
    public static String getDayOfWeek(int dayNumber) {
        String day;
        switch (dayNumber) {
            case 1:
                day = "понедельник";
                break;
            case 2:
                day = "вторник";
                break;
            case 3:
                day = "среда";
                break;
            case 4:
                day = "четверг";
                break;
            case 5:
                day = "пятница";
                break;
            case 6:
                day = "суббота";
                break;
            case 7:
                day = "воскресенье";
                break;
            default:
                throw new IndexOutOfBoundsException();
        }
        return "День недели под номером " + dayNumber + " это " + day;
    }

    public static void main(String[] args) {

        System.out.println(getDayOfWeek(3));
    }
}
