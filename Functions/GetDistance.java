package com.HW_1.Functions;

public class GetDistance {
    protected static double getDistance(double x1, double y1, double x2, double y2) {
        return Math.sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
    }

    public static void main(String[] args) {

        System.out.println("Расстояние между двумя точками равно "
                + getDistance(0.0, 0.0, 0.0, 4.0));
    }
}
